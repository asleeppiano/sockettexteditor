export type Styles = Array<Style>

export interface Style {
  id: number
  style: string
}

export type FontSizes = Array<number>

export enum Status {
  VIEWING = 'viewing',
  EDITING = 'editing',
}

export interface Collaborator {
  id: string
  name: string
  status: Status
}
