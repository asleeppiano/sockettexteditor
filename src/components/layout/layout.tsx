import React from 'react'
import Header from '../header/header'
import { stylelist, fontSizes, collaborators, colors } from '../../data'

type Props = {
  children: React.ReactNode
}

function Layout({ children }: Props) {
  return (
    <div>
      <Header collaborators={collaborators} colors={colors} appname="EDITOR" stylelist={stylelist} fontSizes={fontSizes} />
      {children}
    </div>
  )
}
export default Layout
