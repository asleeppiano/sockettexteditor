import React, { useState, useEffect, useCallback } from 'react'
import { useSelector } from 'react-redux'
import {
  Button,
  ButtonGroup,
  MenuItem,
  Intent,
  Menu,
  Icon,
} from '@blueprintjs/core'
import { Select, ItemRenderer, ItemListRenderer } from '@blueprintjs/select'
import { Style, Styles, Collaborator as CollaboratorType } from '../../types'
import Collaborator from '../collaborator/collaborator'

type Props = {
  appname: string
  stylelist: Styles
  fontSizes: string[]
  collaborators: Array<CollaboratorType>
  colors: string[]
}

type Range = {
  index?: number
  length?: number
}

type Format = {
  [key: string]: any
}

type ColorMenuItem = {
  active: boolean
  disabled: boolean
  color: string
  onClick: any
}

const FontStyle = Select.ofType<Style>()
const FontSizeSelect = Select.ofType<string>()
const ColorSelect = Select.ofType<string>()

function ColorMenuItem({ active, disabled, color, onClick }: ColorMenuItem) {
  return (
    <div
      key={color}
      onClick={onClick}
      className={`color color_${color} ${disabled ? 'disabled' : ''}`}
    >
      {active ? (
        <Icon className="color_active" color="white" icon="small-tick" />
      ) : (
        ''
      )}
    </div>
  )
}
const renderStyle: ItemRenderer<Style> = (
  style,
  { handleClick, modifiers }
) => {
  if (!modifiers.matchesPredicate) {
    return null
  }
  return (
    <MenuItem
      label={style.style}
      active={modifiers.active}
      disabled={modifiers.disabled}
      key={style.id}
      onClick={handleClick}
      text={style.style}
    />
  )
}

const renderFontSizes: ItemRenderer<string> = (
  fontSize,
  { handleClick, modifiers }
) => {
  if (!modifiers.matchesPredicate) {
    return null
  }
  return (
    <MenuItem
      active={modifiers.active}
      disabled={modifiers.disabled}
      key={fontSize.toString()}
      onClick={handleClick}
      text={fontSize}
    />
  )
}

const renderColors: ItemRenderer<string> = (
  color,
  { handleClick, modifiers }
) => {
  if (!modifiers.matchesPredicate) {
    return null
  }
  return (
    <ColorMenuItem
      color={color}
      active={modifiers.active}
      disabled={modifiers.disabled}
      key={color}
      onClick={handleClick}
    />
  )
}
const renderColorMenu: ItemListRenderer<string> = ({
  items,
  itemsParentRef,
  query,
  renderItem,
}) => {
  const renderedItems = items.map(renderItem).filter(item => item != null)
  return <div className="color-select-menu">{renderedItems}</div>
}

function Header({
  appname,
  stylelist,
  fontSizes,
  collaborators,
  colors,
}: Props) {
  const [color, setColor] = useState(colors[0])
  const [style, setStyle] = useState(stylelist[0])
  const [fontSize, setFontSize] = useState(fontSizes[1])
  const [bold, setBold] = useState(false)
  const [italic, setItalic] = useState(false)
  const [underline, setUnderline] = useState(false)
  const [align, setAlign] = useState({
    left: true,
    right: false,
    center: false,
  })
  const [uList, setUList] = useState(false)

  const quill = useSelector((state: any) => state.initEditor.instance)

  const selectStyle = (style: Style) => {
    setStyle(style)
    const s = style.style
    if (quill) {
      quill.format('header', s === 'normal' ? '' : s[s.length - 1])
    }
  }

  const selectFontSize = (fontSize: string) => {
    setFontSize(fontSize)
    if (quill) {
      quill.format('size', fontSize === 'normal' ? false : fontSize)
      console.log('SIZE', fontSize)
      console.log('SIZE FORMAT', quill.getFormat())
    }
  }

  const selectColor = (color: string) => {
    setColor(color)
    if (quill) {
      quill.format('color', color)
      console.log('COLOR', color)
    }
  }

  useEffect(() => {
    function selectionChangeHandler(
      range: Range,
      oldRange: Range,
      source: string
    ) {
      if (quill && range) {
        const format: Format = quill.getFormat(range)
        console.log('format', format)
        if (format.align) {
          setAlign({
            left: false,
            right: format['align'] === 'right' ? true : false,
            center: format['align'] === 'center' ? true : false,
          })
          console.log('format align', format.align)
        } else {
          setAlign({
            left: true,
            right: false,
            center: false,
          })
        }
        setColor(format['color'] || 'black')
        setBold(format['bold'] || false)
        setItalic(format['italic'] || false)
        setUnderline(format['underline'] || false)
      }
    }
    if (quill) quill.on('selection-change', selectionChangeHandler)

    return () => {
      if (quill) quill.off('selection-change', selectionChangeHandler)
    }
  }, [quill])

  function handleActiveColorChange(
    activeItem: string,
    isCreateNewItem: boolean
  ) {
    setColor(activeItem)
  }

  function toggleBold() {
    quill.format('bold', quill.getFormat().bold ? false : true)
    setBold(quill.getFormat().bold)
    console.log('BOLD', quill.getFormat())
  }
  function toggleItalic() {
    quill.format('italic', quill.getFormat().italic ? false : true)
    setItalic(quill.getFormat.italic)
  }
  function toggleUnderline() {
    quill.format('underline', quill.getFormat().underline ? false : true)
    setUnderline(quill.getFormat().underline)
  }
  function toggleAlignRight() {
    if (align.right) {
      return
    }
    quill.format('align', 'right')
    setAlign({ left: false, right: true, center: false })
  }
  function toggleAlignCenter() {
    if (align.center) {
      return
    }
    quill.format('align', 'center')
    setAlign({ left: false, right: false, center: true })
    console.log('ALIGN CENTER', quill.getFormat())
  }
  function toggleAlignLeft() {
    if (align.left) {
      return
    }
    quill.format('align', '')
    setAlign({ left: true, right: false, center: false })
  }
  function toggleUList() {
    quill.format('list', uList ? false : 'bullet')
    setUList(quill.getFormat().list || false)
    console.log('LIST', quill.getFormat())
    console.log('LIST', uList)
  }
  return (
    <div className="header">
      <h1 className="header__appname"> {appname} </h1>
      <FontStyle
        className="header__style-select"
        filterable={false}
        noResults={<MenuItem text="hello" />}
        items={stylelist}
        onItemSelect={selectStyle}
        itemRenderer={renderStyle}
      >
        <Button text={style.style} rightIcon="double-caret-vertical" />
      </FontStyle>
      <FontSizeSelect
        className="header__font-size"
        filterable={false}
        noResults={<MenuItem text="hello" />}
        items={fontSizes}
        onItemSelect={selectFontSize}
        itemRenderer={renderFontSizes}
      >
        <Button text={fontSize} rightIcon="double-caret-vertical" />
      </FontSizeSelect>
      <div className="header-controls">
        <ButtonGroup className="header__btn-group">
          <Button
            intent={bold ? Intent.PRIMARY : Intent.NONE}
            onClick={toggleBold}
            icon="bold"
          ></Button>
          <Button
            intent={italic ? Intent.PRIMARY : Intent.NONE}
            onClick={toggleItalic}
            icon="italic"
          ></Button>
          <Button
            intent={underline ? Intent.PRIMARY : Intent.NONE}
            onClick={toggleUnderline}
            icon="underline"
          ></Button>
          <ColorSelect
            activeItem={color}
            onActiveItemChange={handleActiveColorChange}
            filterable={false}
            noResults={<MenuItem text="no colors" />}
            items={colors}
            onItemSelect={selectColor}
            itemRenderer={renderColors}
            itemListRenderer={renderColorMenu}
          >
            <Button icon="style"></Button>
          </ColorSelect>
          <Button
            intent={align.left ? Intent.PRIMARY : Intent.NONE}
            onClick={toggleAlignLeft}
            icon="align-left"
          ></Button>
          <Button
            intent={align.center ? Intent.PRIMARY : Intent.NONE}
            onClick={toggleAlignCenter}
            icon="align-center"
          ></Button>
          <Button
            intent={align.right ? Intent.PRIMARY : Intent.NONE}
            onClick={toggleAlignRight}
            icon="align-right"
          ></Button>
          <Button
            intent={uList ? Intent.PRIMARY : Intent.NONE}
            onClick={toggleUList}
            icon="properties"
          ></Button>
        </ButtonGroup>
      </div>
      <div className="header-collaborators">
        {collaborators && collaborators.length > 0
          ? collaborators.map(collaborator => (
              <Collaborator
                key={collaborator.id}
                name={collaborator.name}
                status={collaborator.status}
              />
            ))
          : ''}
      </div>
    </div>
  )
}

export default Header
