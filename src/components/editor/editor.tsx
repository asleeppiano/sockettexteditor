import React, { useState, useRef } from 'react'
import { Button, InputGroup } from '@blueprintjs/core'
import 'quill/dist/quill.core.css'
import quill from 'quill'
import { bindActionCreators, Dispatch, ActionCreatorsMapObject } from 'redux'
import { connect } from 'react-redux'
import { EditorState } from '../../reducers/editor'
import { EditorAction, InitEditor } from '../../actions/editor'

import * as Y from 'yjs'
import { WebsocketProvider } from 'y-websocket'
import { QuillBinding } from 'y-quill'
import QuillCursors from 'quill-cursors'

type Props = {}
type State = {}

function mapStateToProps(state: any) {
  return { editorInst: state.initEditor.instance }
}

function mapDispatchToProps(dispatch: Dispatch<EditorAction>) {
  return {
    instantiateEditor: (instance: any) => dispatch(InitEditor(instance)),
  }
}

class Editor extends React.Component<Props, State> {
  private editorRef = React.createRef<HTMLDivElement>()
  private editor: any = undefined

  constructor(props: Props) {
    super(props)
  }

  componentDidMount() {
    const options = {
      placeholder: 'Start typing...',
      modules: {
        cursors: true,
      },
    }
    quill.register('modules/cursors', QuillCursors)
    const ydoc = new Y.Doc()
    const provider = new WebsocketProvider('ws://localhost:1234', 'quill', ydoc)
    const docType = ydoc.getText('quill')
    const textEditor = new (quill as any)('#editor-ta', options)

    this.props.instantiateEditor(textEditor)
    const binding = new QuillBinding(docType, textEditor, provider.awareness)
  }

  render() {
    return (
      <div className="editor">
        <input
          placeholder="TITLE"
          className="editor__title"
          name="title"
          type="text"
        />
        <div className="editor-container">
          <div
            className="editor-container__quill"
            ref={this.editorRef}
            id="editor-ta"
          ></div>
        </div>
      </div>
    )
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Editor)
