import { Styles, Collaborator, Status } from '../types'

export const collaborators: Array<Collaborator> = [
    {
      id: "1",
      name: 'Andrey',
      status: Status.EDITING
    },{
      id: '2',
      name: 'Kate',
      status: Status.VIEWING
    }
  ]

export const colors = ["black", "blue", "green", "red"]

export const stylelist: Styles = [
  {
    id: 1,
    style: 'normal',
  },
  {
    id: 2,
    style: 'heading1',
  },
  {
    id: 3,
    style: 'heading2',
  },
  {
    id: 4,
    style: 'heading3',
  },
]

export const fontSizes: string[] = [
  'small',
  'normal',
  'large',
  'huge'
]

