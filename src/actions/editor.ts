import { INIT_EDITOR } from '../constants'

export interface InitEditor {
  type: INIT_EDITOR
  instance: any
}

export type EditorAction = InitEditor

export function InitEditor(instance: any): InitEditor {
  return {
    type: INIT_EDITOR,
    instance,
  }
}
