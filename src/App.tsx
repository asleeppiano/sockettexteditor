import React from 'react'
import './App.scss'
import Layout from './components/layout/layout'
import Editor from './components/editor/editor'
import {Provider} from 'react-redux'
import store from './store'
// import { Provider } from 'react-redux'
// import store from './store'
// import { BrowserRouter as Router, Route } from 'react-router-dom'

function App() {
  return (
    <Provider store={store}>
      <Layout>
        <Editor/>
      </Layout>
    </Provider>
  )
}

export default App
