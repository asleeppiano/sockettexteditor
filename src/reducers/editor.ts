import { INIT_EDITOR } from '../constants'
import { EditorAction } from '../actions/editor'

export type EditorState = {
  instance: any
}

const initialState: EditorState = {
  instance: null,
}

export function initEditor(state = initialState, action: EditorAction) {
  switch (action.type) {
    case INIT_EDITOR:
      return { instance: action.instance }
    default:
      return state
  }
}
