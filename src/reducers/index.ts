import { combineReducers } from 'redux'
import { initEditor } from './editor'

export default combineReducers({
  initEditor
})
